inp_txt = open('input.txt', 'r')
input = inp_txt.readlines()
text = []
#list
num = ['nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh','sebelas']
def proses(baris):
		x = int(baris)
		# satuan
		if x <= 11:
			return (num[int(x)])
		# belasan
		elif x > 11 and x < 20:
			return (num[int(x % 10)] + ' belas')
		# puluhan
		elif x >= 20 and x < 100: 
			return (proses(x // 10) + ' puluh ' + proses(x % 10))
		# ratusan < 200
		elif x >= 100 and x < 200:
			return ('seratus ' + proses(x % 100))
		# ratusan < 1000
		elif x >= 200 and x < 1000:
			return (proses(x // 100) + ' ratus ' + proses(x % 100))
		# ribuan < 2000
		elif x >= 1000 and x < 2000:
			return ('seribu ' + proses(x % 1000))
		# ribuan
		elif x >= 2000 and x < 10000:
			return (proses(x // 1000) + ' ribu ' + proses(x % 1000))
		# puluhan ribu
		elif x <= 100000:
			return (proses(x // 1000) + ' ribu ' + proses(x % 1000))
		
def desimal(baris):
	baris = str(baris)
	if '.' in baris:
		koma = ''
		for a in baris[baris.index('.')+1:]:
			a = int(a)
			koma += '{} '.format(num[a])
		return(proses(baris[:baris.index('.')]) +' koma '+ koma)
	else:
			return proses(baris)

for baris in input:
	baris = baris.split()
	for a in range (len(baris)):
		baris[a] = list(baris[a])
		for b in range (len(baris[a])):
			if baris[a][b] == ',' and b!=len(baris[a])-1:
				baris[a][b] = '.'
		baris[a] = ''.join(baris[a])
	for a in range(len(baris)):
		try:
			if baris[a].isdigit():
				baris[a] = desimal(baris[a])
			elif baris[a][0:-1].isdigit():
				baris[a] = desimal(baris[a][0:-1]) + '.'
			elif float(baris[a]):
				baris[a] = desimal(baris[a])
		except ValueError:
			continue
	baris = " ".join(baris)
	text.append(baris)

inp_txt.close()
text='\n'.join(text)
out_txt = open('output.txt', 'w')
out_txt.write(text)
out_txt.close()